//
//  NewsfeedTableViewCell.swift
//  VkNewsFeed
//
//  Created by Иван Абрамов on 04.09.2020.
//  Copyright © 2020 Иван Абрамов. All rights reserved.
//

import UIKit

protocol FeedCellViewModel {
    var iconUrlString: String { get }
    var name: String  { get }
    var date: String { get }
    var text: String? { get }
    var likes: Int? { get }
    var comments: Int? { get }
    var shares: Int? { get }
    var views: Int? { get }
}

class NewsfeedTableViewCell: UITableViewCell {

    static let cellId = "NewsfeedTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var postLabel: UILabel!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var sharesLabel: UILabel!
    @IBOutlet weak var viewsLabel: UILabel!
    
    func set(viewModel: FeedCellViewModel) {
        self.nameLabel.text = viewModel.name
        self.dateLabel.text = viewModel.date
        self.postLabel?.text = viewModel.text
        self.likesLabel.text = String(viewModel.likes ?? 0)
        self.commentsLabel.text = String(viewModel.comments ?? 0)
        self.sharesLabel.text = String(viewModel.shares ?? 0)
        self.viewsLabel.text = String(viewModel.views ?? 0)
    }
}
