//
//  NewsfeedPresenter.swift
//  VkNewsFeed
//
//  Created by Иван Абрамов on 02.09.2020.
//  Copyright (c) 2020 Иван Абрамов. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol NewsfeedPresentationLogic {
  func presentData(response: Model.Response.ResponseType)
}

class NewsfeedPresenter: NewsfeedPresentationLogic {
    weak var viewController: NewsfeedDisplayLogic?
  
//  MARK: Present Data
  
    func presentData(response: Model.Response.ResponseType) {
        switch response {
            case .presentNewsFeed(feed: let feed):
                let cells = feed.items.map { (item) in
                    return cellViewModel(from: item)
                }
                
                let feedViewModel = FeedViewModel.init(cells: cells)
                viewController?.displayModel(viewModel: .displayNewsFeed(model: feedViewModel))
        }
    }
    
    private func cellViewModel(from item: FeedItem) -> FeedViewModel.Cell {
        return FeedViewModel.Cell.init(
            iconUrlString: "urlString",
            name: "",
            date: "future date",
            text: item.text,
            likes: item.likes?.count,
            comments: item.comments?.count,
            shares: item.reposts?.count,
            views: item.views?.count
        )
    }
    
}
