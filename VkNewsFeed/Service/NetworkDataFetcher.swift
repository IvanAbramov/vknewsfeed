//
//  NetworkDataFetcher.swift
//  VkNewsFeed
//
//  Created by Иван Абрамов on 01.09.2020.
//  Copyright © 2020 Иван Абрамов. All rights reserved.
//

import Foundation

protocol DataFetcher {
    func getFeed(response: @escaping (FeedResponse?) -> Void)
}

struct NetworkDataFetcher: DataFetcher {
    
    let networking : Networking
    
    init(networking : Networking) {
        self.networking = networking
    }
    
    func getFeed(response: @escaping (FeedResponse?) -> Void) {
          let params = ["filters": "post,photo"]
          networking.request(path: API.newsFeed, params: params) { (data, error) in
              if let error = error {
                  print("Error: \(error.localizedDescription)")
              } else {
                
                let result = self.decodeJSON(type: FeedReponseWrapped.self, data: data)
                  
                response(result?.response)
                
              }
          }
      }
    
    private func decodeJSON<T: Decodable>(type: T.Type, data: Data?) -> T? {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        guard let data = data, let result = try? decoder.decode(T.self, from: data) else {
            return nil
        }
        
        return result
    }
}
